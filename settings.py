import os
import json

class Settings:
	isValid = True
	lastError = ""

	def __init__(self):
		self.resetData()
		self.load()

	def resetData(self):
		self.settings = {}
		self.settings["profiles"] = {}
		self.settings["selectedProfile"] = ""

	def load(self):
		try:
			with open('settings.json') as json_data:
				self.settings = json.load(json_data)
		except:
			print("Couldn't load settings file.")

	def save(self):
		try:
			with open('settings.json', 'w') as json_data:
				json.dump(self.settings, json_data)
		except:
			print("Couldn't load settings file.")

	def createProfile(self, profile):
		if profile in self.settings["profiles"]:
			return
		self.settings["profiles"][profile] = {}
		self.settings["profiles"][profile]["ATS_path"] = ""
		self.settings["profiles"][profile]["train_path"] = ""
		self.settings["profiles"][profile]["extension"] = ""

	def getProfiles(self):
		return [name for name in self.settings["profiles"]]

	def getSelectedProfile(self):
		return self.settings["selectedProfile"]

	def setSelectedProfile(self, profile):
		self.settings["selectedProfile"] = profile

	def getATSPath(self, profile):
		if profile in self.settings["profiles"]:
			return self.settings["profiles"][profile]["ATS_path"]
		else:
			return ""

	def setATSPath(self, profile, path):
		if profile in self.settings["profiles"]:
			self.settings["profiles"][profile]["ATS_path"] = path

	def getTrainPath(self, profile):
		if profile in self.settings["profiles"]:
			return self.settings["profiles"][profile]["train_path"]
		else:
			return ""

	def setTrainPath(self, profile, path):
		if profile in self.settings["profiles"]:
			self.settings["profiles"][profile]["train_path"] = path

	def getSelectedExtension(self, profile):
		if profile in self.settings["profiles"]:
			return self.settings["profiles"][profile]["extension"]
		else:
			return "None"

	def setSelectedExtension(self, profile, extension):
		if profile in self.settings["profiles"]:
			self.settings["profiles"][profile]["extension"] = extension


			