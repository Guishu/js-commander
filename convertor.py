import json
import pprint
import inflection
import codecs

#List of element not needing a closing tag
singletonElements = frozenset(["area ","base","br","col","command","embed","hr","img","input","link","meta","param","source"])

#Keep track of origin browser
originBrowser = "";

#Generate custom style to handle browser compatibility issues
def generateCustomStyle(generated, depth):
    generated.append('<style>')
    if(originBrowser == "chrome"):
        generated.append('button::-moz-focus-inner{border: 0;padding: 0;}')
    generated.append('</style>')

#recursively write elements/texts
def handleElement(element, generated, depth):
    if(element['element'] == "_label"):
        #Just generate text
        if('text' in element):
            generated.append(element['text'])
    else:
        #Open tag
        generated.append("<" + element['element'])

        #Add style
        generated.append(" style='")
        for prop,value in element['style'].items():
            generated.append(inflection.dasherize(inflection.underscore(prop)))
            generated.append(':')
            generated.append(value)
            generated.append(';')
        generated.append("' ")

        #If element is an image, set its src
        if element['element'].lower() == "img":
            generated.append('src="')
            generated.append(element['src'])
            generated.append('" ')

        #If element is a TD, it can have rowspan/colspan
        if element['element'].lower() == "td":
            if 'rowSpan' in element:
                generated.append('rowspan="')
                generated.append(str(element['rowSpan']))
                generated.append('" ')
            if 'colSpan' in element:
                generated.append('colspan="')
                generated.append(str(element['colSpan']))
                generated.append('" ')

        #If element is an input get its type
        if element['element'].lower() == "input" and 'type' in element:
            generated.append('type="')
            generated.append(str(element['type']))
            generated.append('" ')

        #If element is HTML, save browser
        if element['element'].lower() == "html" and 'browser' in element:
            global originBrowser
            originBrowser = element['browser'];

        #If element can have children generate then then close,otherwise just slashclose
        if element['element'].lower() in singletonElements:
            generated.append("/>")
        else:
            #Close begin tag
            generated.append(">")

            #For head element, generate a style tag according to origin browser
            if element['element'].lower() == "head":
                generateCustomStyle(generated, depth + 1)

            #Generate children
            for child in element['children']:
                handleElement(child, generated, depth + 1)

            #Add end tag
            generated.append("</" + element['element'] + ">")


################Entry point###################
#Open file and load its content as json
f = open('aaa.out', 'r')
content = json.loads(f.read())
f.close()

#Concatenating is faster on lists
generated_list = []

#Generate list of elements
handleElement(content, generated_list, 0);

#Dump html file
output = codecs.open('aaa.out.html', 'w', 'utf-8')
output.write(''.join(generated_list))
output.close()
