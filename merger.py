import os
import shutil
import stat
import time
import threading
import subprocess

js_files = [
	["specificity.js", -1],
	["rectangle.js", -1],
	["utilities.js", -1],
	["download.js", -1],
	["globalcss.js", -1],
	["events.js", -1],
	["style.js", -1],
	["combobox.js", -1],
	["extension.js", -1],
	["properties.js", -1],
	["main.js", -1]
]

extension_files = {}

def mergeJSGrabber(ATSPath, trainPath):
	src_path = os.path.join(ATSPath, "Src", "JSGrabber")
	dest_path = os.path.join(trainPath, "Client", "Loader", "testd")
	dest_file = os.path.join(dest_path, "GrabbingScript.js")

	with open(dest_file, 'w') as outfile:
		for js_file in js_files:
			js_file[1] = os.path.getmtime(os.path.join(src_path, js_file[0]))
			with open(os.path.join(src_path, js_file[0])) as infile:
				outfile.write(infile.read())

def list_extensions(ATSPath):
	jsgrabber_extensions_path = os.path.join(ATSPath, "Host", "Vimago_Files", "Files", "Applications")
	try:
		return [d for d in os.listdir(jsgrabber_extensions_path) if os.path.isdir(os.path.join(jsgrabber_extensions_path, d))]
	except:
		return []

def rmtree_readonly_workaround(action, name, exc):
	os.chmod(name, stat.S_IWRITE)
	os.remove(name)

def mergeExtension(ATSPath, extension, trainPath):
	#Copy extension folder
	src_path = os.path.join(ATSPath, "Host", "Vimago_Files", "Files", "Applications", extension)
	dest_extension = os.path.join(trainPath, "Client", "Loader", "testd", extension)
	if os.path.isdir(dest_extension):
		shutil.rmtree(dest_extension, onerror=rmtree_readonly_workaround)
	shutil.copytree(src_path, dest_extension)

	#Save timestamp for copied files
	for dirName, subdirs, files in os.walk(src_path):
		for file in files:
			file_path = os.path.join(dirName, file)
			extension_files[file_path] = os.path.getmtime(file_path)

	#Configure RecordConfig file
	grabConfig = ''
	with open('RecordConfiguration.xml') as grabConfigFile:
		grabConfig = grabConfigFile.read()
	grabConfig = grabConfig.replace('#Signature#', extension).replace('#DisplayName#', extension)

	#Save RecordConfig on the right location
	dest_config = os.path.join(trainPath, "Client", "Loader", "testd", "RecordConfiguration.xml")
	with open(dest_config, 'w') as grabConfigDestFile:
		grabConfigDestFile.write(grabConfig)

def should_import_js_grabber(ATSPath):
	grabber_folder = os.path.join(ATSPath, "Src", "JSGrabber")
	for js_file in js_files:
		if js_file[1] == -1 or os.path.getmtime(os.path.join(grabber_folder, js_file[0])) != js_file[1]:
			return True
	return False

def should_import_extension(ATSPath, extension):
	if extension == 'None':
		return False
	ext_path = os.path.join(ATSPath, "Host", "Vimago_Files", "Files", "Applications", extension)
	for dirName, subdirs, files in os.walk(ext_path):
		for file in files:
			file_path = os.path.join(dirName, file)
			if not file_path in extension_files:
				extension_files[file_path] = os.path.getmtime(file_path)
				return True
			elif os.path.getmtime(file_path) != extension_files[file_path]:
				return True
	return False

def launch(trainPath):
	exe = os.path.join(trainPath, "Client", "Loader", "testd", "SimulationRecord.exe")
	if os.path.isfile(exe):
		CREATE_NO_WINDOW = 0x08000000
		subprocess.call(exe, creationflags=CREATE_NO_WINDOW)

class Watcher(threading.Thread):
	def __init__(self, ATSPath, extension, trainPath, logger):
		threading.Thread.__init__(self)
		self.ATSPath = ATSPath
		self.extension = extension
		self.trainPath = trainPath
		self.logger = logger
		self.watching = False

	def run(self):
		self.watching = True
		while self.watching:
			time.sleep(1)
			if should_import_js_grabber(self.ATSPath):
				self.logger("Merging JS Grabber ...")
				mergeJSGrabber(self.ATSPath, self.trainPath)
				self.logger(" done\n", False)
			if should_import_extension(self.ATSPath, self.extension):
				self.logger("Merging extension " + self.extension + " ...")
				mergeExtension(self.ATSPath, self.extension, self.trainPath)
				self.logger(" done\n", False)

	def stop(self):
		self.watching = False


