from tkinter import *
from tkinter.ttk import *
from tkinter.scrolledtext import *

from settings import Settings
import merger
import datetime

class JSCommander:

	def __init__(self):
		self.settings = Settings()

		self.window = Tk()
		self.setupData()
		self.setupInterface()
		self.setupEvents()
		self.refreshValues()

		self.watcher = None
		
		self.window.mainloop()

	def setupData(self):
		self.editATSValue = StringVar()
		self.editTrainValue = StringVar()

	def setupInterface(self):
		self.window.title("JS Commander")
		self.window.iconbitmap(".\\app.ico")

		Label(self.window, text="Profile:").grid(row=0, column=2, pady=5, sticky=E)
		self.comboProfile = Combobox(self.window)
		self.comboProfile.grid(row=0, column=3, columnspan=2, sticky=E+W)
		self.buttonNew = Button(self.window, text="New", command=self.onNewProfile, width=5)
		self.buttonNew.grid(row=0, column=5, sticky=W, padx=5)


		Label(self.window, text="ATS root path:").grid(row=1, column=1, columnspan=2, pady=5, sticky=E)
		self.editATS = Entry(self.window, textvariable=self.editATSValue)
		self.editATS.grid(row=1, column=3, columnspan=6, sticky=E+W)

		Label(self.window, text="Train root path:").grid(row=2, column=1, columnspan=2, pady=5, sticky=E)
		self.editTrain = Entry(self.window, textvariable=self.editTrainValue)
		self.editTrain.grid(row=2, column=3, columnspan=6, sticky=E+W)

		Label(self.window, text="Extensions:").grid(row=3, column=1, columnspan=2, pady=5, sticky=E)
		self.comboExtension = Combobox(self.window)
		self.comboExtension.grid(row=3, column=3, columnspan=6, sticky=E+W)

		self.buttonMerge = Button(self.window, text="Merge", command=self.merge)
		self.buttonMerge.grid(row="5", column="3", padx="30", sticky=E+W)

		self.buttonWatch = Button(self.window, text="Watch", command=self.watch)
		self.buttonWatch.grid(row="5", column="4", sticky=E+W)

		self.buttonLaunch = Button(self.window, text="JS grabber", command=self.launch)
		self.buttonLaunch.grid(row="5", column="5", padx="30", sticky=E+W)

		self.log = ScrolledText(self.window, height="5", width="25", state=DISABLED, wrap=NONE)
		self.log.grid(row=6, column="1", columnspan="7", pady="5", sticky=E+W)
		self.log.configure(font=("Lucida Console", 8))

		col_count, row_count = self.window.grid_size()
		for col in range(col_count+1):
		    self.window.grid_columnconfigure(col, minsize=20, weight=1)
		for row in range(row_count+1):
		    self.window.grid_rowconfigure(row, minsize=20, weight=1)


	def setupEvents(self):
		self.window.protocol("WM_DELETE_WINDOW", self.onClose)
		self.editATSValue.trace("w", lambda name, index, mode, value=self.editATSValue.get(): self.onATSPathValueChange(value))
		self.editTrainValue.trace("w", lambda name, index, mode, value=self.editTrainValue.get(): self.onTrainPathValueChange(value))
		self.comboProfile.bind("<<ComboboxSelected>>", self.onProfileValueChanged)
		self.comboExtension.bind("<<ComboboxSelected>>", self.onExtensionValueChanged)

	def refreshValues(self):
		self.resetProfileList()
		self.editATSValue.set(self.settings.getATSPath(self.settings.getSelectedProfile()))
		self.editTrainValue.set(self.settings.getTrainPath(self.settings.getSelectedProfile()))
		self.resetExtensionList()

	def onNewProfile(self):
		if self.comboProfile.get():
			self.settings.createProfile(self.comboProfile.get())
			self.settings.setSelectedProfile(self.comboProfile.get())
			self.settings.save()
			self.refreshValues()

	def onClose(self):
		if self.buttonWatch["text"] != "Watch":
			self.watch()
		self.window.destroy()

	def onATSPathValueChange(self, value):
		self.settings.setATSPath(self.comboProfile.get(), self.editATSValue.get())
		self.settings.save()
		self.resetExtensionList()

	def onTrainPathValueChange(self, value):
		self.settings.setTrainPath(self.comboProfile.get(), self.editTrainValue.get())
		self.settings.save()

	def resetProfileList(self):
		profiles = self.settings.getProfiles()
		selected = self.settings.getSelectedProfile()
		self.comboProfile["values"] = profiles
		if selected and selected in profiles:
			self.comboProfile.set(selected)

	def resetExtensionList(self):
		extensions = ["None"] + merger.list_extensions(self.editATSValue.get())
		self.comboExtension["values"] = extensions
		selected = self.settings.getSelectedExtension(self.comboProfile.get())
		if selected in extensions:
			self.comboExtension.set(selected)
		else:
			self.comboExtension.set("None")

	def onProfileValueChanged(self, *args):
		self.settings.setSelectedProfile(self.comboProfile.get())
		self.settings.save()
		self.refreshValues()

	def onExtensionValueChanged(self, *args):
		self.settings.setSelectedExtension(self.comboProfile.get(), self.comboExtension.get())
		self.settings.save()

	def merge(self):
		self.logData("Merging JS Grabber ...")
		merger.mergeJSGrabber(self.editATSValue.get(), self.editTrainValue.get())
		self.logData(" done\n", False)
		extension = self.comboExtension.get()
		if(extension != "None"):
			self.logData("Merging Extension " + extension + " ...")
			merger.mergeExtension(self.editATSValue.get(), extension, self.editTrainValue.get())
			self.logData(" done\n", False)

	def watch(self):
		if self.buttonWatch["text"] == "Watch":
			self.buttonWatch["text"] = "Stop"
			self.comboProfile["state"]="disabled"
			self.buttonNew["state"]="disabled"
			self.editATS["state"]="disabled"
			self.editTrain["state"]="disabled"
			self.comboExtension["state"]="disabled"
			self.buttonMerge["state"]="disabled"
			self.watcher = merger.Watcher(self.editATSValue.get(), self.comboExtension.get(), self.editTrainValue.get(), self.logData)
			self.watcher.start()
		else:
			self.watcher.stop()
			self.watcher.join()
			self.watcher = None
			self.buttonWatch["text"] = "Watch"
			self.comboProfile["state"]="normal"
			self.buttonNew["state"]="normal"
			self.editATS["state"]="normal"
			self.editTrain["state"]="normal"
			self.comboExtension["state"]="normal"
			self.buttonMerge["state"]="normal"

	def launch(self):
		merger.launch(self.editTrainValue.get())

	def logData(self, data, timestamp=True):
		self.log["state"]=NORMAL
		if timestamp:
			self.log.insert(END, datetime.datetime.now().strftime("[%H:%M:%S]") + " " + data)
		else:
			self.log.insert(END, data)
		self.log["state"]=DISABLED

myCommander = JSCommander()